package main

// devoops_bot. A telegram bot
// Copyright (C) 2017  Edward Larkey <edwlarkey at gmail dot com>

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

import (
	"log"
	"os"
	"regexp"
	"strconv"
	"strings"

	"github.com/asdine/storm"
	"gopkg.in/telegram-bot-api.v4"
)

type Karma struct {
	ID    int    `storm:"id,increment"`
	Name  string `storm:"unique"`
	Karma int
}

var telegram_api_key = os.Getenv("TELEGRAM_API_KEY")

func increment(db *storm.DB, item string) error {
	var karma Karma

	err := db.One("Name", item, &karma)

	if err == storm.ErrNotFound {
		karma = Karma{Name: item, Karma: 1}

		err = db.Save(&karma)
		if err != nil {
			log.Panic(err)
		}
	} else {
		karma.Karma++

		err = db.Update(&karma)
		if err != nil {
			log.Panic(err)
		}
	}

	return nil
}

func get_karma(db *storm.DB, item string) string {
	var karma Karma
	err := db.One("Name", item, &karma)

	if err != nil {
		log.Panic(err)
	}

	return strconv.Itoa(karma.Karma)

}

func decrement(db *storm.DB, item string) error {
	var karma Karma

	err := db.One("Name", item, &karma)

	if err == storm.ErrNotFound {
		karma = Karma{Name: item, Karma: -1}

		err = db.Save(&karma)
		if err != nil {
			log.Panic(err)
		}
	} else {
		karma.Karma--

		err = db.Update(&karma)
		if err != nil {
			log.Panic(err)
		}
	}

	return nil
}

func main() {
	db, err := storm.Open("devoops.db")
	if err != nil {
		log.Panic(err)
	}

	defer db.Close()

	bot, err := tgbotapi.NewBotAPI(telegram_api_key)
	if err != nil {
		log.Panic(err)
	}

	bot.Debug = true

	log.Printf("Authorized on account %s", bot.Self.UserName)

	u := tgbotapi.NewUpdate(0)
	u.Timeout = 60

	updates, err := bot.GetUpdatesChan(u)

	for update := range updates {
		if update.Message == nil {
			continue
		}

		log.Printf("[%s] %s", update.Message.From.UserName, update.Message.Text)

		r, _ := regexp.Compile("@?[a-z,A-Z,\\-,\\_,]+\\+\\+")
		increments := r.FindAllString(update.Message.Text, -1)

		d, _ := regexp.Compile("@?[a-z,A-Z,\\-,\\_,]+\\-\\-")
		decrements := d.FindAllString(update.Message.Text, -1)

		for _, handle := range increments {
			handle = strings.TrimSuffix(handle, "++")
			err := increment(db, handle)
			if err != nil {
				log.Panic(err)
			}

			level := get_karma(db, handle)

			msg := tgbotapi.NewMessage(update.Message.Chat.ID, handle+" gained a level! ("+level+")")
			bot.Send(msg)
		}

		for _, handle := range decrements {
			handle = strings.TrimSuffix(handle, "--")
			err := decrement(db, handle)
			if err != nil {
				log.Panic(err)
			}

			level := get_karma(db, handle)

			msg := tgbotapi.NewMessage(update.Message.Chat.ID, handle+" dun goofed! ("+level+")")
			bot.Send(msg)
		}
	}
}
